import '../node_modules/searchkit/theming/theme.scss';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import ElasticApp  from './ElasticApp';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

ReactDOM.render( 
  <ElasticApp />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
