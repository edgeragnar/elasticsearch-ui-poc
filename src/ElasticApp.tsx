import * as React from 'react';
import * as _ from 'lodash';
import {
    SearchkitManager,
    SearchkitProvider,
    Layout,
    TopBar
    , SearchBox,
    LayoutBody,
    SideBar,
    DynamicRangeFilter,
    RangeFilter,
    RefinementListFilter
    , LayoutResults,
    ActionBar,
    HitsStats,
    ActionBarRow,
    SelectedFilters,
    ResetFilters,
    NoHits,
    Pagination,
    ViewSwitcherHits
} from 'searchkit';
const searchkit = new SearchkitManager('https://search-develastic6upgraded-uskiu3e5yiza3cyn5rdw35lv44.us-east-1.es.amazonaws.com/enrollmentsextended');

// const MovieHitsGridItem = (props: any) => {
//     const {bemBlocks, result} = props;
//     let url = 'http://www.imdb.com/title/' + result._source.imdbId;
//     const source: any = _.extend({}, result._source, result.highlight);
//     return (
//       <div className={bemBlocks.item().mix(bemBlocks.container('item'))} data-qa="hit">
//         <a href={url} target="_blank">
//           <img data-qa="poster" className={bemBlocks.item('poster')} src={result._source.poster} width="170" height="240"/>
//           <div data-qa="title" className={bemBlocks.item('title')} dangerouslySetInnerHTML={{__html: source.title}}/>

//         </a>
//       </div>
//     );
//   };
const EnrollmentsList = (props: any) => {
    const { bemBlocks, result } = props;
    const source: any = _.extend({}, result._source, result.highlight);
    return (
        <div className={bemBlocks.item().mix(bemBlocks.container('item'))} data-qa="hit">

            <div>
                <div>name:{source.userFullName}</div>
                <div>course:{source.courseName}</div>
                <div>school:{source.schoolName}</div>
                <div>teachers:{source.teachers}</div>
                <div>progress:{source.percentByTime}</div>
                <div>overall:{source.overallGrade}</div>
                <div>active:{source.activeTime}</div>
                <div>date enrolled:{source.dateEnrolled}</div>
                <div>last gradebook:{source.lastGradebookEntry}</div>
            </div>
        </div>
    );
};
// const Enrollments = (props: any) => {
//     const { bemBlocks, result } = props;
//     const source: any = _.extend({}, result._source, result.highlight);

//     return (
//         <div className={bemBlocks.item().mix(bemBlocks.container('item'))} data-qa="hit">

//             <div>name:{source.userFullName}</div>
//             <div>course:{source.courseName}</div>
//             <div>school:{source.schoolName}</div>
//             <div>teachers:{source.teachers}</div>
//             <div>progress:{source.percentByTime}</div>
//             <div>overall:{source.overallGrade}</div>
//             <div>active:{source.activeTime}</div>
//             <div>date enrolled:{source.dateEnrolled}</div>
//             <div>last gradebook:{source.lastGradebookEntry}</div>
//         </div>
//     );
// };
class ElasticApp extends React.Component {
    render() {
        return (
            <SearchkitProvider searchkit={searchkit}>
                <Layout>
                    <TopBar>
                        <SearchBox autofocus={true} searchOnChange={true} prefixQueryFields={['schoolName', 'schoolID', 'courseName', 'userID']} />
                    </TopBar>
                    <LayoutBody>

                        <SideBar>
                            {/* <HierarchicalMenuFilter
                                fields={['type.raw', 'genres.raw']}
                                title="Categories"
                                id="categories" 
                            /> */}
                            <RefinementListFilter
                                id="schoolIDs"
                                title="School ID"
                                field="schoolID"
                                size={10}
                            />
                            {/* <DynamicRangeFilter field="percentByTime" id="PercentByTime" title="Progress" rangeFormatter={(count) => count + '*'} /> */}
                            <RangeFilter id="percentByTime" field="percentByTime" title="Progress" min={0} max={100} showHistogram={true} />
                            <RefinementListFilter field="userFullName.keyword" id="userfullname" title="userfullname" size={10} />
                            <RefinementListFilter field="userGroups.keyword" id="UserGroups" title="UserGroups" size={10} />
                            <RefinementListFilter field="subject.keyword" id="subject" title="subject" size={10} />
                            <RefinementListFilter field="studentGradeLevel.keyword" id="studentGradeLevel" title="Student" size={10} />
                            <RefinementListFilter field="seriesArray.keyword" id="SeriesArray" title="Series" size={10} />
                            <RangeFilter id="overallGrade" field="overallGrade" title="Overall Grade" min={0} max={100} showHistogram={true} />
                            <RangeFilter id="relativeGrade" field="relativeGrade" title="Relative Grade" min={0} max={100} showHistogram={true} />
                            <RangeFilter id="actualGrade" field="actualGrade" title="Actual Grade" min={0} max={100} showHistogram={true} />

                            <RefinementListFilter field="eLL.keyword" id="ell" title="Special Populations" size={10} />

                            <DynamicRangeFilter field="activeTime" id="activetime" title="Active Time" rangeFormatter={(count) => count + '*'} />

                        </SideBar>
                        <LayoutResults>
                            <ActionBar>

                                <ActionBarRow>
                                    <HitsStats />
                                </ActionBarRow>

                                <ActionBarRow>
                                    <SelectedFilters />
                                    <ResetFilters />
                                </ActionBarRow>

                            </ActionBar>
                            <ViewSwitcherHits
                                hitsPerPage={100}
                                hitComponents={
                                    [
                                        { key: 'list', title: 'list', itemComponent: EnrollmentsList }
                                    ]
                                }
                            />
                            {/* <Hits mod="sk-hits-grid" hitsPerPage={100} itemComponent={Enrollments} /> */}
                            <NoHits />
                            <Pagination showNumbers={true}/>
                        </LayoutResults>
                    </LayoutBody>
                </Layout>
            </SearchkitProvider>
        );
    }
}

export default ElasticApp;
