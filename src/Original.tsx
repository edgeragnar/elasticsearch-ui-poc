import * as React from 'react';
import * as _ from 'lodash';
import {
    SearchkitManager,
    SearchkitProvider,
    Layout,
    TopBar
    , SearchBox,
    LayoutBody,
    SideBar,
    HierarchicalMenuFilter,
    RefinementListFilter
    , LayoutResults,
    ActionBar,
    HitsStats,
    ActionBarRow,
    SelectedFilters,
    ResetFilters,
    NoHits,
    Hits
} from 'searchkit';
const searchkit = new SearchkitManager('http://demo.searchkit.co/api/movies/');

const MovieHitsGridItem = (props: any) => {
    const {bemBlocks, result} = props;
    let url = 'http://www.imdb.com/title/' + result._source.imdbId;
    const source: any = _.extend({}, result._source, result.highlight);
    return (
      <div className={bemBlocks.item().mix(bemBlocks.container('item'))} data-qa="hit">
        <a href={url} target="_blank">
          <img data-qa="poster" className={bemBlocks.item('poster')} src={result._source.poster} width="170" height="240"/>
          <div data-qa="title" className={bemBlocks.item('title')} dangerouslySetInnerHTML={{__html: source.title}}/>
          
        </a>
      </div>
    );
  };

class ElasticApp extends React.Component {
    render() {
        return (
            <SearchkitProvider searchkit={searchkit}>
                <Layout>
                    <TopBar>
                        <SearchBox autofocus={true} searchOnChange={true} prefixQueryFields={['actors^1', 'type^2', 'languages', 'title^10']} />
                    </TopBar>
                    <LayoutBody>
                        
                        <SideBar>
                            <HierarchicalMenuFilter
                                fields={['type.raw', 'genres.raw']}
                                title="Categories"
                                id="categories" 
                            />
                            <RefinementListFilter
                                id="actors"
                                title="Actors"
                                field="actors.raw"
                                operator="AND"
                                size={10} 
                            />
                        </SideBar>
                        <LayoutResults>
                            <ActionBar>

                                <ActionBarRow>
                                    <HitsStats />
                                </ActionBarRow>

                                <ActionBarRow>
                                    <SelectedFilters />
                                    <ResetFilters />
                                </ActionBarRow>

                            </ActionBar>
                            <Hits mod="sk-hits-grid" hitsPerPage={10} itemComponent={MovieHitsGridItem} sourceFilter={['title', 'poster', 'imdbId']}/>
                            <NoHits />
                        </LayoutResults>
                    </LayoutBody>
                </Layout>
            </SearchkitProvider>
        );
    }
}

export default ElasticApp;
